RSpec.describe GreetingPeople do
  subject { Object.include(GreetingPeople).new }

  describe '#say_hello' do
    context 'nobody in particular' do
      it 'gives a single generic hello' do
        expect(subject.say_hello).to include 'Hello everyone'
      end
    end

    context 'one or more specific people' do
      let(:alice) { Person.new 'Alice' }
      let(:bob) { Person.new 'Bob' }

      it 'greets each one individually' do
        expect(subject.say_hello(alice, bob)).to include(
          'Hello Alice',
          'Hello Bob'
        )
      end
    end
  end
end