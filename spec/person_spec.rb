RSpec.describe Person do
  describe '#introduction' do
    context 'at the office' do
      subject { Professional.new 'Cathy' }
      
      it 'introduces herself professionally' do
        expect(subject.introduction).to eq 'My name is Cathy'
      end
    end

    context 'in the club' do
      subject { PartyGoer.new 'Cathy' }

      it 'gives a lively introduction' do
        expect(subject.introduction).to eq "Hey! I'm Cathy"
      end
    end

    context 'in some vast unknown' do
      subject { Person.new 'Cathy' }

      it 'just cannot find the words' do
        expect { subject.introduction }.to raise_error NotImplementedError
      end
    end
  end
end