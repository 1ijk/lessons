module GreetingPeople
  GENERIC_AUDIENCE = [ Person.new('everyone') ].freeze

  def say_hello(*people)
    people = GENERIC_AUDIENCE if people.empty?
    people.map { |person| "Hello #{person.name}" }
  end
end