class Person
  attr_reader :name

  def initialize(name)
    @name = name
  end

  def introduction
    [introduction_phrase, name].join ' '
  end

  private

  def introduction_phrase
    raise NotImplementedError
  end
end